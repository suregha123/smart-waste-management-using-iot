 
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

ESP8266WebServer server(80); // 80 is the port number

const char* ssid="smart";
const char* password="password";

String Website,data="GETTING THE SENSOR DATA";

void WebsiteContent(){
    
    Website="<html>\n";
    Website+="<style>\n";
    Website+="#div1{\n";
    Website+="width:400px;\n";
    Website+="margin:0 auto;\n";
    Website+="margin-top:130px;\n";
    Website+="font-size:300%;\n";
    Website+="color:powderblue;\n";
    Website+="}\n";
    Website+="</style>\n";
    Website+="<body onload='process()'>";
    Website+="<div id='div1'>"+data+"</div></body></html>";
    server.send(200,"text/html",Website);
  }


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  WiFi.begin(ssid,password);
  while(WiFi.status()!=WL_CONNECTED)delay(500);
  WiFi.mode(WIFI_STA);
  Serial.print(WiFi.localIP());
  server.on("/",WebsiteContent);
  server.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  
  
  server.handleClient();
}     
    
